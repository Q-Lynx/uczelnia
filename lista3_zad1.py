
def liczba_szczesliwa():
    counter = 0
    for i in range(100000):
        if '777' in str(i):
            pierwsza = czy_pierwsza(i)
            if pierwsza is True:
                print(i)
                counter += 1
    return counter


def czy_pierwsza(i):
    for n in range(2, int(i/2)+1):
        if (i % n) == 0:
            pierwsza = False
        else:
            pierwsza = True
        return pierwsza


if __name__ == '__main__':
    counter = liczba_szczesliwa()
    print("Jest ", counter, " takich liczb.")
