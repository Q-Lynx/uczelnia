def kółka(n=4):
    lista = list()
    przerwa = n
    for i in range(n):
        rzad = ' ' * przerwa + '#' * n
        n += 2
        przerwa -= 1
        lista.append(rzad)
    return lista


if __name__ == '__main__':
    n = 8
    lista = kółka(n)
    for i in lista:
        print(i)
    for i in lista:
        print('#' * 3 * n)
    for i in lista[::-1]:
        print(i)

#równanie okręgu (x+a)^2 + (y-b)^2 =< r
# (a,b) - środek okręgu
# r - promień