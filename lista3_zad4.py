from turtle import fd, bk, rt, lt, pensize, speed, penup, pendown
from numpy import sin
pensize(1)
speed('fastest')

def rysuj(y=0):
    penup()
    bk(100)
    pendown()
    for i in range(126):
        fd(3)
        lt(90)
        fd(sin(y)*80)
        rt(90)
        fd(3)
        rt(90)
        fd(sin(y)*80)
        lt(90)
        bk(3)
        y += 0.05


rysuj()


input()
