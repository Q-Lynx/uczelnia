
def szachownica(n, k):
    for t in range(2*n):
        if t % 2 == 0:
            for i in range(1, n):
                line = ' ' * k + '#' * k
                print(line * n)
        else:
            for i in range(1, n):
                line = '#' * k + ' ' * k
                print(line * n)


if __name__ == '__main__':
    szachownica(4, 3)
