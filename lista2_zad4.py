from duze_cyfry import daj_cyfre


def split(liczba):
    return [cyfra for cyfra in liczba]


def wypisz_litery(liczba):
    a = []
    b = []
    c = []
    d = []
    e = []
    for r in liczba:
        i = daj_cyfre(int(r))
        a.append(i[0])
        b.append(i[1])
        c.append(i[2])
        d.append(i[3])
        e.append(i[4])

    a = ' '.join([str(elem) for elem in a])
    b = ' '.join([str(elem) for elem in b])
    c = ' '.join([str(elem) for elem in c])
    d = ' '.join([str(elem) for elem in d])
    e = ' '.join([str(elem) for elem in e])
    return a, b, c, d, e


if __name__ == '__main__':
    liczba = input(str("podaj liczbe "))
    liczba = split(liczba)
    a, b, c, d, e = wypisz_litery(liczba)
    print(a)
    print(b)
    print(c)
    print(d)
    print(e)


