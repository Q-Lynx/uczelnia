def potega(a, n):
    wynik = 1  # zmienna lokalna
    for i in range(n):
        wynik = wynik * a  # albo: wynik *= a
    return wynik


def kwadrat(n):
    for i in range(n):
        for j in range(n):
            print("* ", end="")
        print()


def kwadrat2(n):
    for i in range(n):
        print(n * "# ")


for i in range(5):
    print("Przebieg:", i)
    print(20 * "-")
    kwadrat(3 + 2 * i)
    print()

for i in range(5):
    print("Przebieg:", i + 5)
    print(20 * "-")
    kwadrat2(3 + i)
    print()  # pusty wiersz



