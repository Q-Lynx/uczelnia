from losowanie_fragmentow import losuj_fragment


def losuj_haslo(n):
    haslo = str()
    while len(haslo) < n:
        wyraz = losuj_fragment()
        if len(haslo+wyraz) == n-1:
            break
        haslo += wyraz
    if len(haslo) == n:
        return haslo
    else:
        pass


def losuj_10_hasel(n):
    for i in range(10):
        haslo = losuj_haslo(n)
        while haslo is None:
            haslo = losuj_haslo(n)
        print(haslo)


if __name__ == '__main__':
    print("Hasła ośmioliterowe:")
    losuj_10_hasel(8)
    print("Hasła dwunastoliterowe:")
    losuj_10_hasel(12)
