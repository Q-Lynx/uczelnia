
def koperta(n=15):
    wzor_koperta = list()
    dlugosc = 2 * n + 1
    przerwa = 0
    for i in range(int((dlugosc - 2)/2) + 1):
        środek = (dlugosc - 2) - (2*przerwa)
        linia = '*' + (' ' * przerwa) + '*' + (' ' * środek) + '*' + (' ' * przerwa) + '*'
        print(len(linia))
        wzor_koperta.append(linia)
        przerwa += 1
    return dlugosc, wzor_koperta,


def printowanie(dlugosc, wzor_koperta):
    print('*' * dlugosc)
    for i in wzor_koperta:
        print(i)
    linia_środek = '*' + (' ' * int(dlugosc / 2)) + '*' + (' ' * int(dlugosc / 2)) + '*'
    print(linia_środek)
    for i in wzor_koperta[::-1]:
        print(i)
    print('*' * dlugosc)


if __name__ == '__main__':
    dlugosc, wzor_koperta = koperta(8)
    printowanie(dlugosc, wzor_koperta)

