

def liczba_cyfr(n):
    silnia = 1
    cyfry = 1
    for i in range(1, n+1):
        silnia *= i
    while silnia > 10:
        silnia /= 10
        cyfry += 1
    return cyfry


if __name__ == '__main__':
    n = 100
    for i in range(1, n+1):
        cyfry = liczba_cyfr(i)
        if cyfry == 1:
            print(i, '! ma ', cyfry, ' cyfrę.')
        if cyfry < 5:
            if cyfry > 1:
                print(i, '! ma ', cyfry, ' cyfry.')
        else:
            print(i, '! ma ', cyfry, ' cyfr.')
